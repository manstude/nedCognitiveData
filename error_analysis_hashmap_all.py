import os, sys
from tutorials.utils import score_predictions
import pandas as pd
import ujson as json
import jsonlines
from tqdm import tqdm
from collections import defaultdict
from IPython.core.display import display, HTML, Markdown
from bootleg.symbols.entity_symbols import EntitySymbols
from bootleg.symbols.type_symbols import TypeSymbols
from bootleg.symbols.kg_symbols import KGSymbols
import numpy as np
def printmd(string):
    display(Markdown(string))
tqdm.pandas()
display(HTML("<style>.container { width:90% !important; }</style>"))
pd.options.display.max_colwidth = 500
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', 5000)




def accuracy(df, crc_col="gold_qid", pred_col="pred_qid"):
    total = df.shape[0]
    correct = df[df[crc_col] == df[pred_col]].shape[0]
    return correct/total

def get_incorrect(df, crc_col="gold_qid", pred_col="pred_qid"):
    return df[df[crc_col] != df[pred_col]]

def num_incorrect(df, crc_col="gold_qid", pred_col="pred_qid"):
    return df[df[crc_col] != df[pred_col]].shape[0]

def print_incorrect_over_total(df, crc_col="gold_qid", pred_col="pred_qid"):
    num_in = df[df[crc_col] != df[pred_col]].shape[0]
    total = df.shape[0]
    print(f"{num_in} / {total} = {num_in/total}")

def errors_by_type(df, type_sys, pred_col="pred_qid"):
    errors_type = defaultdict(int)
    df["correct"] = df["gold_qid"] == df[pred_col]
    for r in df.iterrows():
        row = r[1]
        if row.correct is True:
            continue
        for t in row[type_sys]:
            errors_type[t] += 1
    return errors_type



input_dir = '/mnt/ds3lab-scratch/manstude/zuco_wiki_subset_0305_0_-1_final'
emb_dir = '/mnt/ds3lab-scratch/manstude/emb_data'
entity_dump = EntitySymbols(load_dir=os.path.join(input_dir, "entity_db/entity_mappings"))
types_hy = TypeSymbols(entity_dump, emb_dir, max_types=3, type_vocab_file="hyena_vocab_1229.json", type_file="hyena_types_1229.json")
types_wd = TypeSymbols(entity_dump, emb_dir, max_types=3, type_vocab_file="wikidatatitle_to_typeid_1229.json", type_file="wikidata_types_1229.json")
types_rel = TypeSymbols(entity_dump, emb_dir, max_types=50, type_vocab_file="relation_to_typeid_1229.json", type_file="kg_relation_types_1229.json")
kg_syms = KGSymbols(entity_dump, emb_dir, "kg_adj_1229.txt")
a2q = json.load(open(os.path.join(input_dir, "entity_db/entity_mappings/alias2qids.json")))
q2title = entity_dump._qid2title


'''
qid2cnt = defaultdict(int)


with jsonlines.open(os.path.join(input_dir, "train.jsonl")) as in_f:
    for line in in_f:
        for qid in line["qids"]:
            qid2cnt[qid] += 1
with open(os.path.join(input_dir, "train_qidcnt_occurrences.json"), "w") as out_f:
    json.dump(dict(qid2cnt), out_f)
'''



with open(os.path.join(input_dir, "train_qidcnt_occurrences.json"), "r") as in_f:
    qid2cnt = json.load(in_f)


# ALL Hashmap - ET
pred_file_1 = '/mnt/ds3lab-scratch/manstude/bootleg-internal/logs_guid/logs_guid/hashmap_et_all/2021_04_06/07_49_21/2bdd0cb7/test/best_model_NED_Bootleg_dev_final_loss_acc_boot.model/bootleg_labels.jsonl'
#pred_file_2 = ''
#pred_file_3 = ''

res_1 = score_predictions(orig_file=f'{input_dir}/test.jsonl',
                 pred_file=pred_file_1,
                 title_map=q2title,
                 cands_map=a2q,
                 type_symbols=[types_hy, types_wd, types_rel],
                 kg_symbols=[kg_syms])

res_2 = score_predictions(orig_file=f'{input_dir}/test.jsonl',
                 pred_file=pred_file_2,
                 title_map=q2title,
                 cands_map=a2q,
                 type_symbols=[types_hy, types_wd, types_rel],
                 kg_symbols=[kg_syms])

res_3 = score_predictions(orig_file=f'{input_dir}/test.jsonl',
                 pred_file=pred_file_3,
                 title_map=q2title,
                 cands_map=a2q,
                 type_symbols=[types_hy, types_wd, types_rel],
                 kg_symbols=[kg_syms])

print('### ALL Hashmap - ET ###')

res_1["num_cands"] = res_1["cands"].apply(lambda x: len(x))
res_1["qid_cnt"] = res_1["gold_qid"].apply(lambda x: qid2cnt.get(x, 0))
res_1 = res_1[(res_1["is_gold_label"]) & (res_1["num_cands"] > 1)]
'''
res_2["num_cands"] = res_2["cands"].apply(lambda x: len(x))
res_2["qid_cnt"] = res_2["gold_qid"].apply(lambda x: qid2cnt.get(x, 0))
res_2 = res_2[(res_2["is_gold_label"]) & (res_2["num_cands"] > 1)]

res_3["num_cands"] = res_3["cands"].apply(lambda x: len(x))
res_3["qid_cnt"] = res_3["gold_qid"].apply(lambda x: qid2cnt.get(x, 0))
res_3 = res_3[(res_3["is_gold_label"]) & (res_3["num_cands"] > 1)]
'''

# average
vec = np.array([accuracy(res_1, pred_col="pred_qid")])
print("Average, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))

# never
vec = np.array([accuracy(res_1[res_1['qid_cnt'] == 0], pred_col="pred_qid")])
print("0 times, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))

# tail
vec = np.array([accuracy(res_1[(res_1['qid_cnt'] >= 1) & (res_1['qid_cnt'] <= 10)], pred_col="pred_qid")])
print("1-10 times, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))

# 11 - 50
vec = np.array([accuracy(res_1[(res_1['qid_cnt'] >= 11) & (res_1['qid_cnt'] <= 50)], pred_col="pred_qid")])
print("11-50 times, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))

# >50
vec = np.array([accuracy(res_1[(res_1['qid_cnt'] >= 51)], pred_col="pred_qid")])
print(">50 times, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))







# ALL Hashmap - EEG
pred_file_1 = '/mnt/ds3lab-scratch/manstude/bootleg-internal/logs_guid/logs_guid/hashmap_eeg_all/2021_04_06/09_10_45/cfe2c043/test/best_model_NED_Bootleg_dev_final_loss_acc_boot.model/bootleg_labels.jsonl'
#pred_file_2 = ''
#pred_file_3 = ''

res_1 = score_predictions(orig_file=f'{input_dir}/test.jsonl',
                 pred_file=pred_file_1,
                 title_map=q2title,
                 cands_map=a2q,
                 type_symbols=[types_hy, types_wd, types_rel],
                 kg_symbols=[kg_syms])

res_2 = score_predictions(orig_file=f'{input_dir}/test.jsonl',
                 pred_file=pred_file_2,
                 title_map=q2title,
                 cands_map=a2q,
                 type_symbols=[types_hy, types_wd, types_rel],
                 kg_symbols=[kg_syms])

res_3 = score_predictions(orig_file=f'{input_dir}/test.jsonl',
                 pred_file=pred_file_3,
                 title_map=q2title,
                 cands_map=a2q,
                 type_symbols=[types_hy, types_wd, types_rel],
                 kg_symbols=[kg_syms])

print('### ALL Hashmap - EEG ###')

res_1["num_cands"] = res_1["cands"].apply(lambda x: len(x))
res_1["qid_cnt"] = res_1["gold_qid"].apply(lambda x: qid2cnt.get(x, 0))
res_1 = res_1[(res_1["is_gold_label"]) & (res_1["num_cands"] > 1)]
'''
res_2["num_cands"] = res_2["cands"].apply(lambda x: len(x))
res_2["qid_cnt"] = res_2["gold_qid"].apply(lambda x: qid2cnt.get(x, 0))
res_2 = res_2[(res_2["is_gold_label"]) & (res_2["num_cands"] > 1)]

res_3["num_cands"] = res_3["cands"].apply(lambda x: len(x))
res_3["qid_cnt"] = res_3["gold_qid"].apply(lambda x: qid2cnt.get(x, 0))
res_3 = res_3[(res_3["is_gold_label"]) & (res_3["num_cands"] > 1)]
'''

# average
vec = np.array([accuracy(res_1, pred_col="pred_qid")])
print("Average, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))

# never
vec = np.array([accuracy(res_1[res_1['qid_cnt'] == 0], pred_col="pred_qid")])
print("0 times, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))

# tail
vec = np.array([accuracy(res_1[(res_1['qid_cnt'] >= 1) & (res_1['qid_cnt'] <= 10)], pred_col="pred_qid")])
print("1-10 times, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))

# 11 - 50
vec = np.array([accuracy(res_1[(res_1['qid_cnt'] >= 11) & (res_1['qid_cnt'] <= 50)], pred_col="pred_qid")])
print("11-50 times, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))

# >50
vec = np.array([accuracy(res_1[(res_1['qid_cnt'] >= 51)], pred_col="pred_qid")])
print(">50 times, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))





# ALL Hashmap - ET & EEG
pred_file_1 = '/mnt/ds3lab-scratch/manstude/bootleg-internal/logs_guid/hashmap_etANDeeg_all/2021_04_06/07_46_37/68a9303a/test/best_model_NED_Bootleg_dev_final_loss_acc_boot.model/bootleg_labels.jsonl'
#pred_file_2 = ''
#pred_file_3 = ''

res_1 = score_predictions(orig_file=f'{input_dir}/test.jsonl',
                 pred_file=pred_file_1,
                 title_map=q2title,
                 cands_map=a2q,
                 type_symbols=[types_hy, types_wd, types_rel],
                 kg_symbols=[kg_syms])

res_2 = score_predictions(orig_file=f'{input_dir}/test.jsonl',
                 pred_file=pred_file_2,
                 title_map=q2title,
                 cands_map=a2q,
                 type_symbols=[types_hy, types_wd, types_rel],
                 kg_symbols=[kg_syms])

res_3 = score_predictions(orig_file=f'{input_dir}/test.jsonl',
                 pred_file=pred_file_3,
                 title_map=q2title,
                 cands_map=a2q,
                 type_symbols=[types_hy, types_wd, types_rel],
                 kg_symbols=[kg_syms])

print('### ALL Hashmap - ET & EEG ###')

res_1["num_cands"] = res_1["cands"].apply(lambda x: len(x))
res_1["qid_cnt"] = res_1["gold_qid"].apply(lambda x: qid2cnt.get(x, 0))
res_1 = res_1[(res_1["is_gold_label"]) & (res_1["num_cands"] > 1)]
'''
res_2["num_cands"] = res_2["cands"].apply(lambda x: len(x))
res_2["qid_cnt"] = res_2["gold_qid"].apply(lambda x: qid2cnt.get(x, 0))
res_2 = res_2[(res_2["is_gold_label"]) & (res_2["num_cands"] > 1)]

res_3["num_cands"] = res_3["cands"].apply(lambda x: len(x))
res_3["qid_cnt"] = res_3["gold_qid"].apply(lambda x: qid2cnt.get(x, 0))
res_3 = res_3[(res_3["is_gold_label"]) & (res_3["num_cands"] > 1)]
'''

# average
vec = np.array([accuracy(res_1, pred_col="pred_qid")])
print("Average, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))

# never
vec = np.array([accuracy(res_1[res_1['qid_cnt'] == 0], pred_col="pred_qid")])
print("0 times, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))

# tail
vec = np.array([accuracy(res_1[(res_1['qid_cnt'] >= 1) & (res_1['qid_cnt'] <= 10)], pred_col="pred_qid")])
print("1-10 times, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))

# 11 - 50
vec = np.array([accuracy(res_1[(res_1['qid_cnt'] >= 11) & (res_1['qid_cnt'] <= 50)], pred_col="pred_qid")])
print("11-50 times, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))

# >50
vec = np.array([accuracy(res_1[(res_1['qid_cnt'] >= 51)], pred_col="pred_qid")])
print(">50 times, mean: {}, std: {}".format(np.mean(vec), np.std(vec)))