#!/usr/bin/env python
# coding: utf-8

# In[1]:


from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize
from bs4 import BeautifulSoup
from collections import defaultdict
import html
from urllib.parse import quote, unquote
import jsonlines
import json

def load_title_to_qid(title_to_qid_fpath):
    ''' Creating a dictionary (title -> QID) from Bootleg dataset
    
    Args:
        title_to_qid_fpath: (str) path to Bootleg title_to_id dataset
    
    Returns:
        title_to_qid: (dict) dictionary of titles (lower-cased) to qids 
    '''

    print("*****loading title_to_qid mapping*****")

    title_to_qid = defaultdict(set)
    with jsonlines.open(title_to_qid_fpath, 'r') as in_file:
        for item in in_file:
            title = item['title'].lower()
            qid = item['qid']
            title_to_qid[title].add(qid)
    print("*****loading completed*****")
    return title_to_qid

def wiki2Bootleg(wiki_data_path_train, filename_store):
    '''properly formatting the Wiki dataset 
    
    Args:
        wiki_data_path_train: (str) path to Wiki dataset
        filename_store: (str) name of file, in which to store training data
        
    Returns:
        all_proc_lines: (dict) dictionary of sentences with Bootleg alias information
        no_qid: (set) entity mentions that are not contained in Bootleg dataset
        numOfEnt: (int) total number of entity mentions
    '''
    
    all_proc_lines = []
    sent_idx_unq = 0
    #set which contains all the entities which are not included in Bootleg's dataset
    no_qid = set()
    numOfEnt = 0

    for line in open(wiki_data_path_train):
        if(line.startswith("url=")):
            #take only title from URL
            cur_wiki_page = line.split("/")[-1].replace("_", " ")
        else:
            bsoup = BeautifulSoup(line, features="html.parser")
            if(bsoup.text == "\n"):
                continue

            print(f"*****processing sentence #: {sent_idx_unq}*****")

            aliases = []
            spans = []
            qids = []
            parText = '' #keeps track of all the tokens that are prior to the entity
            ptr = 0 #points to the first token index after already processed entity
            end_tag = "/a>"

            tags = bsoup.find_all("a")

            #enumerating over all tags in sentence
            for tag in tags:
                numOfEnt += 1
                #checking if qid is in Bootleg dict
                if(tag["title"].lower() in title_to_qid.keys()):
                    aliases.append(tag["title"].lower())
                    qids.append(list(title_to_qid[tag["title"].lower()])[0])
                    
                    start_idx = tag.sourcepos
                    end_idx = line.find(end_tag, start_idx) + len(end_tag)

                    text2entity = line[0:start_idx]
                    internal = BeautifulSoup(text2entity, features="html.parser")

                    numtok2entity = len(word_tokenize(internal.text))
                    entity_length = len(word_tokenize(tag.text))
                    
                    #print(tag["title"])
                    #print(word_tokenize(bsoup.text)[numtok2entity: numtok2entity+entity_length])
                
                    spans.append([numtok2entity, numtok2entity+entity_length])
                else:
                    #entity mention without Bootleg QID, ignore entity
                    #qids.append(None)
                    no_qid.add(tag["title"].lower())

            #creating new item for each sentence and appending them together
            if(len(aliases)>0):
                new_item = {
                        "sentence": bsoup.text.rstrip("\n"),
                        "sent_idx_unq": sent_idx_unq,
                        "aliases": aliases,
                        "spans": spans,
                        "qids": qids,
                        "gold": [True for i in range(len(aliases))]
                }

                all_proc_lines.append(new_item)
                
                #file.write(f"{new_item}\n")

            sent_idx_unq += 1


    filename = filename_store + '.jsonl'

    with jsonlines.open(filename, 'w') as writer:
        writer.write_all(all_proc_lines) 
                        
    return all_proc_lines, no_qid, numOfEnt



'''Path to corresponding datasets'''

wiki_data_path_train = "/mnt/ds3lab-scratch/manstude/nedCognitiveData/ZuCo_text.txt"
#local: "/home/manu/ETHZ_Bachelor/BScThesis/data/wikipedia_datav1.0/wikipedia.train"
#cluster: "/mnt/ds3lab-scratch/manstude/CognitiveBootleg_Prep/WikiText_ZuCo2"
title_to_qid_mapPath = "/mnt/ds3lab-scratch/manstude/title_to_all_ids_0224.jsonl"
#local: "/home/manu/ETHZ_Bachelor/BScThesis/data/title_to_all_ids.jsonl"
#cluster: "/mnt/ds3lab-scratch/manstude/title_to_all_ids_0224.jsonl"


#obtain title->qid information
title_to_qid = load_title_to_qid(title_to_qid_mapPath)

#turn ZuCo dataset into correct Bootleg format
all_proc_lines, no_qid, numOfEnt = wiki2Bootleg(wiki_data_path_train, 'train_wikiDataset')

print(f"total number of entity mentions: {numOfEnt}, number of entities without QID: {len(no_qid)}")

