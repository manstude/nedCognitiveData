#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import h5py
import os
import torch
import numpy as np
import pickle as pk
import pandas as pd


# In[ ]:


global_sent_idx = 0
gold_dict = {} #key: (sent_idx, word_pos_idx)


# In[ ]:


pca = pk.load(open('ZuCo_PCA.pickle','rb'))


# ### ZuCo 1.0 EEG 

# In[ ]:


path_zuco1_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco1_preprocessed_sep2020/'


# In[ ]:


for file_name in sorted(os.listdir(path_zuco1_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco1_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):
            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])

            for word_idx in range(numWords):

                #word string
                word_obj = file[sent['content'][word_idx][0]]
                word = u''.join(chr(c[0]) for c in word_obj).lower()
                #print(word)
                word = word.replace(",","")
                word = word.replace(".","")
                word = word.replace("(","")
                word = word.replace(")","")
                word = word.replace("\"","")
                #print(word)

                key = (sent_idx, word_idx)
                
                #computing meanEEG
                numsamples = 0 #keeping track of how many entries we have
                if 'rawEEG' in sent.keys() and file[sent['rawEEG'][word_idx][0]].shape != (2,):
                    tmp = [0 for i in range(105)] #used to add up all measurements

                    for i in range(file[sent['rawEEG'][word_idx][0]].shape[0]):
                        #how many different "rawEEG" cells we have

                        if file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape != (1,1): #ensuring not to be NaN
                            #treating each cell separately

                            #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape)
                            #print(sent_idx)

                            for j in range(len(file[file[sent['rawEEG'][word_idx][0]][i][0]][()])):
                                #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]) #105 dim

                                tmp += file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]
                                numsamples += 1



                if(numsamples>0):
                    #adding to hashmap
                    feature_arr = [float(i/numsamples) for i in tmp] #taking average of measurements
                    features = np.array(feature_arr)

                    #print(word)
                    #features = features.astype(np.float)
                    #print(features)
                        
                    if(not any(np.isnan(features))):
                        #including cog data of word in hashmap
                        tmp = pca.transform(pd.DataFrame([features]))[0]
                        if key in gold_dict.keys():
                            gold_dict[key].append(tmp)

                        else:
                            gold_dict[key] = [tmp]

                    else:
                        print("found a problem")


# ### ZuCo 2.0 EEG 

# In[ ]:


global_sent_idx += 300 #ZuCo 1.0 contains 300 sentences in dataset


# In[ ]:


path_zuco2_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco2_preprocessed_sep2020/'


# In[ ]:


for file_name in sorted(os.listdir(path_zuco1_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco1_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):
            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])

            for word_idx in range(numWords):

                #word string
                word_obj = file[sent['content'][word_idx][0]]
                word = u''.join(chr(c[0]) for c in word_obj).lower()
                #print(word)
                word = word.replace(",","")
                word = word.replace(".","")
                word = word.replace("(","")
                word = word.replace(")","")
                word = word.replace("\"","")
                #print(word)

                key = (sent_idx, word_idx)

                #computing meanEEG
                numsamples = 0 #keeping track of how many entries we have
                if 'rawEEG' in sent.keys() and file[sent['rawEEG'][word_idx][0]].shape != (2,):
                    tmp = [0 for i in range(105)] #used to add up all measurements

                    for i in range(file[sent['rawEEG'][word_idx][0]].shape[0]):
                        #how many different "rawEEG" cells we have

                        if file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape != (1,1): #ensuring not to be NaN
                            #treating each cell separately

                            #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape)
                            #print(sent_idx)

                            for j in range(len(file[file[sent['rawEEG'][word_idx][0]][i][0]][()])):
                                #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]) #105 dim

                                tmp += file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]
                                numsamples += 1



                if(numsamples>0):
                    #adding to hashmap
                    feature_arr = [float(i/numsamples) for i in tmp] #taking average of measurements
                    features = np.array(feature_arr)

                    #print(word)
                    #features = features.astype(np.float)
                    #print(features)
                        
                    if(not any(np.isnan(features))):
                        #including cog data of word in hashmap
                        tmp = pca.transform(pd.DataFrame([features]))[0]
                        if key in gold_dict.keys():
                            gold_dict[key].append(tmp)

                        else:
                            gold_dict[key] = [tmp]

                    else:
                        print("found a problem")


# ### Min Max Scaler

# In[ ]:


from sklearn import preprocessing


# In[ ]:


final_gold_dict = {}
for key in gold_dict.keys():
    final_gold_dict[key] = np.mean(preprocessing.minmax_scale(gold_dict[key]), axis=0)


# ### Store Mapping

# In[ ]:


filename_store = "gold_EEG.pickle"


# In[ ]:


file_store = open(filename_store, 'w')
file_store.close()


# In[ ]:


with open(filename_store, 'wb') as f:
        #dumping dict with mapping ((sent_idx, word_pos_idx) -> cognitive data features)
        pk.dump(final_gold_dict, f)
        print(f"hashmap dumped in {filename_store}")

