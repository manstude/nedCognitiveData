#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import h5py
import os
import torch
import numpy as np
import pickle


# In[ ]:


global_sent_idx = 0
gold_dict = {} #key: (sent_idx, word_pos_idx)


# ### ZuCo 1.0 ET

# In[ ]:


path_zuco1_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco2_preprocessed_sep2020/'


# In[ ]:


for file_name in sorted(os.listdir(path_zuco1_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco1_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):

            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])


            for word_idx in range(numWords):
                #ET features
                #initializing ET data variables
                ffd = gd = gpt = sfd = trt = numFix = None
                #ET measurements (FFD, GD, GPT, SFD, TRT, fixPositions)
                ref_ffd = sent['FFD'] #first fixation duration
                ffd = file[ref_ffd[word_idx][0]][()][0,0] if file[ref_ffd[word_idx][0]][()].shape == (1, 1) else None

                ref_gd = sent['GD'] #gaze duration
                gd = file[ref_gd[word_idx][0]][()][0,0] if file[ref_gd[word_idx][0]][()].shape == (1, 1) else None

                ref_gpt = sent['GPT'] #go-past time
                gpt = file[ref_gpt[word_idx][0]][()][0,0] if file[ref_gpt[word_idx][0]][()].shape == (1, 1) else None

                if('SFD' in sent.keys()): #there are words in ZuCo without SFD feature
                    ref_sfd = sent['SFD'] #single fixation duration
                    sfd = file[ref_sfd[word_idx][0]][()][0,0] if file[ref_sfd[word_idx][0]][()].shape == (1, 1) else None

                ref_trt = sent['TRT'] #total reading time
                trt = file[ref_trt[word_idx][0]][()][0,0] if file[ref_trt[word_idx][0]][()].shape == (1, 1) else None

                ref_numFix = sent['nFixations'] #number of word fixations
                numFix = file[ref_numFix[word_idx][0]][()][0,0] if file[ref_numFix[word_idx][0]][()].shape == (1, 1) else None

                feature_arr = [ffd, gd, gpt, sfd, trt, numFix]
                key = (sent_idx + global_sent_idx, word_idx)
                #print(key)

                #skip/flag word if not all measurements are available
                flag = False
                for feature in feature_arr:
                    if feature == None:
                        flag = True

                if not flag:

                    features = np.array([float(trt), float(gpt), float(sfd), float(ffd), float(gd), float(numFix)])
                    #print(features)

                    #including cog data of word in hashmap
                    if key in gold_dict.keys():
                        gold_dict[key].append(features)

                    else:
                        gold_dict[key] = [features]


# ### ZuCo 2.0 ET

# In[ ]:


global_sent_idx += 300 #ZuCo 1.0 contains 300 sentences in dataset


# In[ ]:


path_zuco2_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco2_preprocessed_sep2020/'


# In[ ]:


for file_name in sorted(os.listdir(path_zuco2_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco2_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):

            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])


            for word_idx in range(numWords):
                #ET features
                #initializing ET data variables
                ffd = gd = gpt = sfd = trt = numFix = None
                #ET measurements (FFD, GD, GPT, SFD, TRT, fixPositions)
                ref_ffd = sent['FFD'] #first fixation duration
                ffd = file[ref_ffd[word_idx][0]][()][0,0] if file[ref_ffd[word_idx][0]][()].shape == (1, 1) else None

                ref_gd = sent['GD'] #gaze duration
                gd = file[ref_gd[word_idx][0]][()][0,0] if file[ref_gd[word_idx][0]][()].shape == (1, 1) else None

                ref_gpt = sent['GPT'] #go-past time
                gpt = file[ref_gpt[word_idx][0]][()][0,0] if file[ref_gpt[word_idx][0]][()].shape == (1, 1) else None

                if('SFD' in sent.keys()): #there are words in ZuCo without SFD feature
                    ref_sfd = sent['SFD'] #single fixation duration
                    sfd = file[ref_sfd[word_idx][0]][()][0,0] if file[ref_sfd[word_idx][0]][()].shape == (1, 1) else None

                ref_trt = sent['TRT'] #total reading time
                trt = file[ref_trt[word_idx][0]][()][0,0] if file[ref_trt[word_idx][0]][()].shape == (1, 1) else None

                ref_numFix = sent['nFixations'] #number of word fixations
                numFix = file[ref_numFix[word_idx][0]][()][0,0] if file[ref_numFix[word_idx][0]][()].shape == (1, 1) else None

                feature_arr = [ffd, gd, gpt, sfd, trt, numFix]
                key = (sent_idx + global_sent_idx, word_idx)
                #print(key)

                #skip/flag word if not all measurements are available
                flag = False
                for feature in feature_arr:
                    if feature == None:
                        flag = True

                if not flag:

                    features = np.array([float(trt), float(gpt), float(sfd), float(ffd), float(gd), float(numFix)])
                    #print(features)

                    #including cog data of word in hashmap
                    if key in gold_dict.keys():
                        gold_dict[key].append(features)

                    else:
                        gold_dict[key] = [features]


# ### Min Max Scaler

# In[ ]:


from sklearn import preprocessing


# In[ ]:


final_gold_dict = {}
for key in gold_dict.keys():
    final_gold_dict[key] = np.mean(preprocessing.minmax_scale(gold_dict[key]), axis=0)


# In[ ]:


# sample
final_gold_dict[(7,6)]


# ### Store Mapping 

# In[ ]:


filename_store = "gold_ET.pickle"


# In[ ]:


file_store = open(filename_store, 'w')
file_store.close()


# In[ ]:


with open(filename_store, 'wb') as f:
        #dumping dict with mapping ((sent_idx, word_pos_idx) -> cognitive data features)
        pickle.dump(final_gold_dict, f)
        print(f"hashmap dumped in {filename_store}")

