#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import numpy as np
import h5py
from scipy import io
import pandas as pd
import pickle as pk


# In[ ]:


final_word_dict = pk.load(open('hashmap_EEG_all_zucoOnly.pickle','rb'))


# In[ ]:


path_natspeech_data = '/mnt/ds3lab-scratch/noraho/cognitive-data/broderick2018_eeg/NaturalSpeech/EEG_processed_by_nora/broderick_results/'


# In[ ]:


NS_PCA_dict = pd.DataFrame([]) #used to fit PCA
NS_word_dict = {}
for file_name in os.listdir(path_natspeech_data):
    if file_name.endswith('.mat'):
        print("reading from: " + file_name)
        data = io.loadmat(path_natspeech_data + file_name, squeeze_me=True, struct_as_record=False)['eegData']
    
        for word_obj in data.word:
            word = word_obj.content.lower()
        
            feature_arr = word_obj.meanEEG
            features = np.array(feature_arr)
        
            #only treat words that have not been seen in ZuCo 1.0 or 2.0
            if word not in final_word_dict.keys():
                #including cog data of word in NS hashmap
                if word in NS_word_dict.keys():
                    NS_word_dict[word].append(features)

                else:
                    NS_word_dict[word] = [features] 
                
            NS_PCA_dict = NS_PCA_dict.append(pd.DataFrame([feature_arr]))


# In[ ]:


data = NS_PCA_dict


# In[ ]:


#PCA with 30 components
from sklearn.decomposition import PCA
pcaNS = PCA(30)
pcaNS.fit(data)
#pcaNS.explained_variance_ratio_.sum()


# In[ ]:


from sklearn import preprocessing
for word in NS_word_dict.keys():
    tmp = pcaNS.transform(pd.DataFrame(NS_word_dict[word]))
    final_word_dict[word] = np.mean(preprocessing.minmax_scale(tmp), axis=0)


# In[2]:


file_store = open('NS_PCA.pickle', 'w')
file_store.close()
with open('NS_PCA.pickle', 'wb') as f:
    pk.dump(pcaNS, f)
    print(f"NS_PCA stored")


# In[ ]:


filename_store = "hashmap_EEG_all.pickle"
file_store = open(filename_store, 'w')
file_store.close()
with open(filename_store, 'wb') as f:
    #dumping hashmap with mapping (word -> cognitive data features)
    pk.dump(final_word_dict, f)
    print(f"hashmap dumped in {filename_store}")


# In[ ]:


cogId2vecMap = dict()
word2cogidMap = dict()
i = 0

for word in final_word_dict.keys():
    word2cogidMap[word] = i
    cogId2vecMap[i] = list(final_word_dict[word])
    i += 1


# In[ ]:


#storing hashmap
dataset_name_store = 'cogid2vecMap_eeg_all.pickle'
with open(dataset_name_store, 'wb') as file:
    pk.dump(cogId2vecMap, file)
    
dataset_name_store = 'word2cogidMap_eeg_all.pickle'
with open(dataset_name_store, 'wb') as file:
    pk.dump(word2cogidMap, file)

