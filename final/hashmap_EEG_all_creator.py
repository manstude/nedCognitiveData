#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import numpy as np
import h5py
from scipy import io
import pandas as pd
import pickle as pk


# In[2]:


word_dict = {}


# In[ ]:


pca = pk.load(open('ZuCo_PCA.pickle','rb'))


# ## ZuCo Corpora

# ### ZuCo 1.0 EEG

# In[3]:


path_zuco1_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco1_preprocessed_sep2020/'


# In[4]:


for file_name in sorted(os.listdir(path_zuco1_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco1_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):
            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])

            for word_idx in range(numWords):

                #word string
                word_obj = file[sent['content'][word_idx][0]]
                word = u''.join(chr(c[0]) for c in word_obj).lower()
                word = word.replace(",","")
                word = word.replace(".","")
                word = word.replace("(","")
                word = word.replace(")","")
                word = word.replace("\"","")

            #computing meanEEG
                if 'rawEEG' in sent.keys() and file[sent['rawEEG'][word_idx][0]].shape != (2,):
                    numsamples = 0 #keeping track of how many entries we have
                    tmp = [0 for i in range(105)] #used to add up all measurements

                    for i in range(file[sent['rawEEG'][word_idx][0]].shape[0]):
                        #how many different "rawEEG" cells we have

                        if file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape != (1,1): #ensuring not to be NaN
                            #treating each cell separately

                            #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape)
                            #print(sent_idx)

                            for j in range(len(file[file[sent['rawEEG'][word_idx][0]][i][0]][()])):
                                #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]) #105 dim

                                tmp += file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]
                                numsamples += 1

                    if(numsamples>0):
                        #adding to hashmap
                        feature_arr = [float(i/numsamples) for i in tmp] #taking average of measurements
                        features = np.array(feature_arr)

                        #print(word)
                        #features = features.astype(np.float)
                        #print(features)
                        
                        if(not any(np.isnan(features))):
                            #including cog data of word in hashmap
                            tmp = pca.transform(pd.DataFrame([features]))[0]
                            if word in word_dict.keys():
                                word_dict[word].append(tmp)

                            else:
                                word_dict[word] = [tmp]

                        else:
                            print("found a problem")


# ### ZuCo 2.0 EEG

# In[5]:


path_zuco2_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco2_preprocessed_sep2020/'


# In[6]:


for file_name in sorted(os.listdir(path_zuco2_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco2_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):
            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])

            for word_idx in range(numWords):

                #word string
                word_obj = file[sent['content'][word_idx][0]]
                word = u''.join(chr(c[0]) for c in word_obj).lower()
                word = word.replace(",","")
                word = word.replace(".","")
                word = word.replace("(","")
                word = word.replace(")","")
                word = word.replace("\"","")

            #computing meanEEG
                if 'rawEEG' in sent.keys() and file[sent['rawEEG'][word_idx][0]].shape != (2,):
                    numsamples = 0 #keeping track of how many entries we have
                    tmp = [0 for i in range(105)] #used to add up all measurements

                    for i in range(file[sent['rawEEG'][word_idx][0]].shape[0]):
                        #how many different "rawEEG" cells we have

                        if file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape != (1,1): #ensuring not to be NaN
                            #treating each cell separately

                            #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape)
                            #print(sent_idx)

                            for j in range(len(file[file[sent['rawEEG'][word_idx][0]][i][0]][()])):
                                #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]) #105 dim

                                tmp += file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]
                                numsamples += 1

                    if(numsamples>0):
                        #adding to hashmap
                        feature_arr = [float(i/numsamples) for i in tmp] #taking average of measurements
                        features = np.array(feature_arr)

                        #print(word)
                        #features = features.astype(np.float)
                        #print(features)
                        
                        if(not any(np.isnan(features))):
                            #including cog data of word in hashmap
                            tmp = pca.transform(pd.DataFrame([features]))[0]
                            if word in word_dict.keys():
                                word_dict[word].append(tmp)

                            else:
                                word_dict[word] = [tmp]

                        else:
                            print("found a problem")


# In[9]:


from sklearn import preprocessing
# min max scaling with transformed measurements
final_word_dict = {}
for word in word_dict.keys():
    final_word_dict[word] = np.mean(preprocessing.minmax_scale(word_dict[word]), axis=0)


# ## Natural Speech EEG

# In[12]:


path_natspeech_data = '/mnt/ds3lab-scratch/noraho/cognitive-data/broderick2018_eeg/NaturalSpeech/EEG_processed_by_nora/broderick_results/'


# In[13]:


NS_PCA_dict = pd.DataFrame([]) #used to fit PCA
NS_word_dict = {}
for filename in os.listdir(path_natspeech_data):
    if file_name.endswith('.mat'):
        print("reading from: " + filename)
        data = io.loadmat(path_natspeech_data + filename, squeeze_me=True, struct_as_record=False)['eegData']
    
        for word_obj in data.word:
            word = word_obj.content.lower()
        
            feature_arr = word_obj.meanEEG
            features = np.array(feature_arr)
        
            #only treat words that have not been seen in ZuCo 1.0 or 2.0
            if word not in final_word_dict.keys():
                #including cog data of word in NS hashmap
                if word in NS_word_dict.keys():
                    NS_word_dict[word].append(features)

                else:
                    NS_word_dict[word] = [features] 
                
            NS_PCA_dict = NS_PCA_dict.append(pd.DataFrame([feature_arr]))


# ### PCA for Natural Speech

# In[14]:


data = NS_PCA_dict


# In[15]:


#PCA with 30 components
from sklearn.decomposition import PCA
pcaNS = PCA(30)
pcaNS.fit(data)
#pcaNS.explained_variance_ratio_.sum()


# In[16]:


from sklearn import preprocessing
for word in NS_word_dict.keys():
    tmp = pcaNS.transform(pd.DataFrame(NS_word_dict[word]))
    final_word_dict[word] = np.mean(preprocessing.minmax_scale(tmp), axis=0)


# #### Storing PCA for later use 

# In[17]:


file_store = open('NS_PCA.pickle', 'w')
file_store.close()


# In[19]:


with open('NS_PCA.pickle', 'wb') as f:
    pk.dump(pcaNS, f)
    print(f"NS_PCA stored")


# ## Store Mapping

filename_store = "hashmap_EEG_all.pickle"
file_store = open(filename_store, 'w')
file_store.close()
with open(filename_store, 'wb') as f:
    #dumping hashmap with mapping (word -> cognitive data features)
    pk.dump(final_word_dict, f)
    print(f"hashmap dumped in {filename_store}")




# In[21]:


cogId2vecMap = dict()
word2cogidMap = dict()
i = 0

for word in final_word_dict.keys():
    word2cogidMap[word] = i
    cogId2vecMap[i] = list(final_word_dict[word])
    i += 1


# In[22]:


#storing hashmap
dataset_name_store = 'cogid2vecMap_eeg_all.pickle'
with open(dataset_name_store, 'wb') as file:
    pk.dump(cogId2vecMap, file)
    
dataset_name_store = 'word2cogidMap_eeg_all.pickle'
with open(dataset_name_store, 'wb') as file:
    pk.dump(word2cogidMap, file)

