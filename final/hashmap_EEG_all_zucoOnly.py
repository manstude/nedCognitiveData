#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import numpy as np
import h5py
from scipy import io
import pandas as pd
import pickle as pk


# In[2]:


word_dict = {}


# In[ ]:


pca = pk.load(open('ZuCo_PCA.pickle','rb'))


# ## ZuCo Corpora

# ### ZuCo 1.0 EEG

# In[3]:


path_zuco1_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco1_preprocessed_sep2020/'


# In[4]:


for file_name in sorted(os.listdir(path_zuco1_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco1_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):
            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])

            for word_idx in range(numWords):

                #word string
                word_obj = file[sent['content'][word_idx][0]]
                word = u''.join(chr(c[0]) for c in word_obj).lower()
                word = word.replace(",","")
                word = word.replace(".","")
                word = word.replace("(","")
                word = word.replace(")","")
                word = word.replace("\"","")

            #computing meanEEG
                if 'rawEEG' in sent.keys() and file[sent['rawEEG'][word_idx][0]].shape != (2,):
                    numsamples = 0 #keeping track of how many entries we have
                    tmp = [0 for i in range(105)] #used to add up all measurements

                    for i in range(file[sent['rawEEG'][word_idx][0]].shape[0]):
                        #how many different "rawEEG" cells we have

                        if file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape != (1,1): #ensuring not to be NaN
                            #treating each cell separately

                            #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape)
                            #print(sent_idx)

                            for j in range(len(file[file[sent['rawEEG'][word_idx][0]][i][0]][()])):
                                #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]) #105 dim

                                tmp += file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]
                                numsamples += 1

                    if(numsamples>0):
                        #adding to hashmap
                        feature_arr = [float(i/numsamples) for i in tmp] #taking average of measurements
                        features = np.array(feature_arr)

                        #print(word)
                        #features = features.astype(np.float)
                        #print(features)
                        
                        if(not any(np.isnan(features))):
                            #including cog data of word in hashmap
                            tmp = pca.transform(pd.DataFrame([features]))[0]
                            if word in word_dict.keys():
                                word_dict[word].append(tmp)

                            else:
                                word_dict[word] = [tmp]

                        else:
                            print("found a problem")


# ### ZuCo 2.0 EEG

# In[5]:


path_zuco2_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco2_preprocessed_sep2020/'


# In[6]:


for file_name in sorted(os.listdir(path_zuco2_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco2_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):
            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])

            for word_idx in range(numWords):

                #word string
                word_obj = file[sent['content'][word_idx][0]]
                word = u''.join(chr(c[0]) for c in word_obj).lower()
                word = word.replace(",","")
                word = word.replace(".","")
                word = word.replace("(","")
                word = word.replace(")","")
                word = word.replace("\"","")

            #computing meanEEG
                if 'rawEEG' in sent.keys() and file[sent['rawEEG'][word_idx][0]].shape != (2,):
                    numsamples = 0 #keeping track of how many entries we have
                    tmp = [0 for i in range(105)] #used to add up all measurements

                    for i in range(file[sent['rawEEG'][word_idx][0]].shape[0]):
                        #how many different "rawEEG" cells we have

                        if file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape != (1,1): #ensuring not to be NaN
                            #treating each cell separately

                            #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape)
                            #print(sent_idx)

                            for j in range(len(file[file[sent['rawEEG'][word_idx][0]][i][0]][()])):
                                #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]) #105 dim

                                tmp += file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]
                                numsamples += 1

                    if(numsamples>0):
                        #adding to hashmap
                        feature_arr = [float(i/numsamples) for i in tmp] #taking average of measurements
                        features = np.array(feature_arr)

                        #print(word)
                        #features = features.astype(np.float)
                        #print(features)
                        
                        if(not any(np.isnan(features))):
                            #including cog data of word in hashmap
                            tmp = pca.transform(pd.DataFrame([features]))[0]
                            if word in word_dict.keys():
                                word_dict[word].append(tmp)

                            else:
                                word_dict[word] = [tmp]

                        else:
                            print("found a problem")


# In[9]:


from sklearn import preprocessing
# min max scaling with transformed measurements
final_word_dict = {}
for word in word_dict.keys():
    final_word_dict[word] = np.mean(preprocessing.minmax_scale(word_dict[word]), axis=0)




file_store = open('hashmap_EEG_all_zucoOnly.pickle', 'w')
file_store.close()

with open('hashmap_EEG_all_zucoOnly.pickle', 'wb') as f:
    pk.dump(final_word_dict, f)
    print(f"hashmap EEG all zucoOnly stored")



