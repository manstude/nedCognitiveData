#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pickle as pk
import numpy as np


# In[ ]:


#loading previously created eyetracking hashmap
word_et = pk.load(open('hashmap_ET_all.pickle','rb'))


# In[ ]:


#loading previously created EEG hashmap
word_eeg = pk.load(open('hashmap_EEG_all.pickle','rb'))


# In[ ]:


#only working with words that are available in both hashmaps
keys = [word for word in word_et.keys() if (word in word_eeg.keys())]


# In[ ]:


final_word_dict = {}
for key in keys:
    final_word_dict[key] = np.concatenate([word_et[key], word_eeg[key]])


# ### Storing Mapping 

# In[ ]:


cogId2vecMap = dict()
word2cogidMap = dict()
i = 0

for word in final_word_dict.keys():
    word2cogidMap[word] = i
    cogId2vecMap[i] = list(final_word_dict[word])
    i += 1


# In[ ]:


#storing hashmap
dataset_name_store = 'cogid2vecMap_etANDeeg_all.pickle'
with open(dataset_name_store, 'wb') as file:
    pk.dump(cogId2vecMap, file)
    
dataset_name_store = 'word2cogidMap_etANDeeg_all.pickle'
with open(dataset_name_store, 'wb') as file:
    pk.dump(word2cogidMap, file)

