#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import numpy as np
import h5py
import pickle
import pandas as pd


# In[2]:


word_dict = {}


# In[3]:


pca = pickle.load(open('ZuCo_PCA.pickle','rb'))


# ### ZuCo 1.0 ET & EEG

# In[4]:


path_zuco1_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco1_preprocessed_sep2020/'


# In[5]:


for file_name in sorted(os.listdir(path_zuco1_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco1_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):
            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])

            for word_idx in range(numWords):

                #word string
                word_obj = file[sent['content'][word_idx][0]]
                word = u''.join(chr(c[0]) for c in word_obj).lower()
                #print(word)
                word = word.replace(",","")
                word = word.replace(".","")
                word = word.replace("(","")
                word = word.replace(")","")
                word = word.replace("\"","")
                #print(word)


                #ET features
                #initializing ET data variables
                ffd = gd = gpt = sfd = trt = numFix = None
                #ET measurements (FFD, GD, GPT, SFD, TRT, fixPositions)
                ref_ffd = sent['FFD'] #first fixation duration
                ffd = file[ref_ffd[word_idx][0]][()][0,0] if file[ref_ffd[word_idx][0]][()].shape == (1, 1) else None

                ref_gd = sent['GD'] #gaze duration
                gd = file[ref_gd[word_idx][0]][()][0,0] if file[ref_gd[word_idx][0]][()].shape == (1, 1) else None

                ref_gpt = sent['GPT'] #go-past time
                gpt = file[ref_gpt[word_idx][0]][()][0,0] if file[ref_gpt[word_idx][0]][()].shape == (1, 1) else None

                if('SFD' in sent.keys()): #there are words in ZuCo without SFD feature
                    ref_sfd = sent['SFD'] #single fixation duration
                    sfd = file[ref_sfd[word_idx][0]][()][0,0] if file[ref_sfd[word_idx][0]][()].shape == (1, 1) else None

                ref_trt = sent['TRT'] #total reading time
                trt = file[ref_trt[word_idx][0]][()][0,0] if file[ref_trt[word_idx][0]][()].shape == (1, 1) else None

                ref_numFix = sent['nFixations'] #number of word fixations
                numFix = file[ref_numFix[word_idx][0]][()][0,0] if file[ref_numFix[word_idx][0]][()].shape == (1, 1) else None

                feature_arr = [ffd, gd, gpt, sfd, trt, numFix]

                #skip/flag word if not all measurements are available
                flag = False
                for feature in feature_arr:
                    if feature == None:
                        flag = True



                #computing meanEEG
                if 'rawEEG' in sent.keys() and file[sent['rawEEG'][word_idx][0]].shape != (2,):
                    numsamples = 0 #keeping track of how many entries we have
                    tmp = [0 for i in range(105)] #used to add up all measurements

                    for i in range(file[sent['rawEEG'][word_idx][0]].shape[0]):
                        #how many different "rawEEG" cells we have

                        if file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape != (1,1): #ensuring not to be NaN
                            #treating each cell separately

                            #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape)
                            #print(sent_idx)

                            for j in range(len(file[file[sent['rawEEG'][word_idx][0]][i][0]][()])):
                                #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]) #105 dim

                                tmp += file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]
                                numsamples += 1



                if not flag and (numsamples>0):

                    features_eeg_arr = np.array([float(i/numsamples) for i in tmp]) #taking average of measurements
                    
                    if(not any(np.isnan(features_eeg_arr))):
                        features_eeg_arr = pd.DataFrame(features_eeg_arr.reshape(1, -1))
                        features_eeg = pca.transform(features_eeg_arr)[0] #transforming meanEEG to principal components

                        features_et = [float(trt), float(gpt), float(sfd), float(ffd), float(gd), float(numFix)]
                        features = np.concatenate([features_et, features_eeg])

                        #including cog data of word in hashmap
                        if word in word_dict.keys():
                            word_dict[word].append(features)

                        else:
                            word_dict[word] = [features]


# ### ZuCo 2.0 ET & EEG

# In[ ]:


path_zuco2_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco2_preprocessed_sep2020/'


# In[ ]:


for file_name in sorted(os.listdir(path_zuco2_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco2_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):
            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])

            for word_idx in range(numWords):

                #word string
                word_obj = file[sent['content'][word_idx][0]]
                word = u''.join(chr(c[0]) for c in word_obj).lower()
                #print(word)
                word = word.replace(",","")
                word = word.replace(".","")
                word = word.replace("(","")
                word = word.replace(")","")
                word = word.replace("\"","")
                #print(word)


                #ET features
                #initializing ET data variables
                ffd = gd = gpt = sfd = trt = numFix = None
                #ET measurements (FFD, GD, GPT, SFD, TRT, fixPositions)
                ref_ffd = sent['FFD'] #first fixation duration
                ffd = file[ref_ffd[word_idx][0]][()][0,0] if file[ref_ffd[word_idx][0]][()].shape == (1, 1) else None

                ref_gd = sent['GD'] #gaze duration
                gd = file[ref_gd[word_idx][0]][()][0,0] if file[ref_gd[word_idx][0]][()].shape == (1, 1) else None

                ref_gpt = sent['GPT'] #go-past time
                gpt = file[ref_gpt[word_idx][0]][()][0,0] if file[ref_gpt[word_idx][0]][()].shape == (1, 1) else None

                if('SFD' in sent.keys()): #there are words in ZuCo without SFD feature
                    ref_sfd = sent['SFD'] #single fixation duration
                    sfd = file[ref_sfd[word_idx][0]][()][0,0] if file[ref_sfd[word_idx][0]][()].shape == (1, 1) else None

                ref_trt = sent['TRT'] #total reading time
                trt = file[ref_trt[word_idx][0]][()][0,0] if file[ref_trt[word_idx][0]][()].shape == (1, 1) else None

                ref_numFix = sent['nFixations'] #number of word fixations
                numFix = file[ref_numFix[word_idx][0]][()][0,0] if file[ref_numFix[word_idx][0]][()].shape == (1, 1) else None

                feature_arr = [ffd, gd, gpt, sfd, trt, numFix]

                #skip/flag word if not all measurements are available
                flag = False
                for feature in feature_arr:
                    if feature == None:
                        flag = True



                #computing meanEEG
                if 'rawEEG' in sent.keys() and file[sent['rawEEG'][word_idx][0]].shape != (2,):
                    numsamples = 0 #keeping track of how many entries we have
                    tmp = [0 for i in range(105)] #used to add up all measurements

                    for i in range(file[sent['rawEEG'][word_idx][0]].shape[0]):
                        #how many different "rawEEG" cells we have

                        if file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape != (1,1): #ensuring not to be NaN
                            #treating each cell separately

                            #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()].shape)
                            #print(sent_idx)

                            for j in range(len(file[file[sent['rawEEG'][word_idx][0]][i][0]][()])):
                                #print(file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]) #105 dim

                                tmp += file[file[sent['rawEEG'][word_idx][0]][i][0]][()][j]
                                numsamples += 1



                if not flag and (numsamples>0):

                    features_eeg_arr = np.array([float(i/numsamples) for i in tmp]) #taking average of measurements
                    
                    if(not any(np.isnan(features_eeg_arr))):
                        features_eeg_arr = pd.DataFrame(features_eeg_arr.reshape(1, -1))
                        features_eeg = pca.transform(features_eeg_arr)[0] #transforming meanEEG to principal components

                        features_et = [float(trt), float(gpt), float(sfd), float(ffd), float(gd), float(numFix)]
                        features = np.concatenate([features_et, features_eeg])

                        #including cog data of word in hashmap
                        if word in word_dict.keys():
                            word_dict[word].append(features)

                        else:
                            word_dict[word] = [features]


# ### Min Max Scaler

# In[ ]:


from sklearn import preprocessing


# In[ ]:


final_word_dict = {}
for word in word_dict.keys():
    final_word_dict[word] = np.mean(preprocessing.minmax_scale(word_dict[word]), axis=0)


# ### Store Mapping

# In[ ]:


cogId2vecMap = dict()
word2cogidMap = dict()
i = 0

for word in final_word_dict.keys():
    word2cogidMap[word] = i
    cogId2vecMap[i] = list(final_word_dict[word])
    i += 1


# In[ ]:


#storing hashmap
dataset_name_store = 'cogid2vecMap_etANDeeg_zuco.pickle'
with open(dataset_name_store, 'wb') as file:
    pickle.dump(cogId2vecMap, file)
    
dataset_name_store = 'word2cogidMap_etANDeeg_zuco.pickle'
with open(dataset_name_store, 'wb') as file:
    pickle.dump(word2cogidMap, file)

