#!/usr/bin/env python
# coding: utf-8

# In[23]:


import pandas as pd
import os
import numpy as np
import csv
import h5py
import pickle as pk


# In[2]:


word_dict = {}


# ### GECO

# In[3]:


path_geco_data = '/mnt/ds3lab-scratch/noraho/cognitive-data/geco_gaze/MonolingualReadingData.xlsx'
path_geco_sent = '/mnt/ds3lab-scratch/noraho/cognitive-data/geco_gaze/EnglishMaterial.xlsx'


# In[4]:


data = pd.read_excel(path_geco_data, usecols="A, J, K, M, T, AC, AZ, BB")


# In[5]:


sent_info = pd.read_excel(path_geco_sent, usecols="A, B, C, D")


# In[6]:


subjects = pd.unique(data['PP_NR'].values)
sentences = pd.unique(sent_info['SENTENCE_ID'].values)


# In[7]:


for subj in [subjects[0]]:
    print(subj)
    subj_data_orig = data.loc[data['PP_NR'] == subj]
    df_subj = pd.DataFrame(columns=['sentence_idx','word_pos_idx','word_id_orig','word','TRT', 'GPT', 'SFD', 'FFD', 'GD'])
    for sent_idx, sent in enumerate(sentences):
        word_ids = sent_info["WORD_ID"].loc[sent_info['SENTENCE_ID'] == sent].values
        #print(word_ids)
        tokens = sent_info["WORD"].loc[sent_info['SENTENCE_ID'] == sent].values
        #print(tokens)
        for word_pos_index, (tok, id) in enumerate(zip(tokens, word_ids)):
            trt = subj_data_orig['WORD_TOTAL_READING_TIME'].loc[subj_data_orig['WORD_ID'] == id].values
            trt = 0 if trt == "." or len(trt) == 0 else trt
            
            gpt = subj_data_orig['WORD_GO_PAST_TIME'].loc[subj_data_orig['WORD_ID'] == id].values
            gpt = 0 if gpt == "." or len(gpt) == 0 else gpt
            
            ffd = subj_data_orig['WORD_FIRST_FIXATION_DURATION'].loc[subj_data_orig['WORD_ID'] == id].values
            ffd = 0 if ffd == "." or len(ffd) == 0 else ffd
            
            gd = subj_data_orig['WORD_GAZE_DURATION'].loc[subj_data_orig['WORD_ID'] == id].values
            gd = 0 if gd == "." or len(gd) == 0 else gd
            
            numFix = subj_data_orig['WORD_FIXATION_COUNT'].loc[subj_data_orig['WORD_ID'] == id].values
            numFix = 0 if numFix == "." or len(numFix) == 0 else numFix
            
            #creating additional feature: single fixation duration
            ff = subj_data_orig['WORD_FIRST_FIXATION_DURATION'].loc[subj_data_orig['WORD_ID'] == id].values
            sfd = 0 if (numFix != 1 or (ff == "." or len(ff) == 0)) else ff
            
            
            word = str(tok).lower()
            
            features = np.array([float(trt), float(gpt), float(sfd), float(ffd), float(gd), float(numFix)])
            
            #including cog data of word in hashmap
            if word in word_dict.keys():
                word_dict[word].append(features)
                    
            else:
                word_dict[word] = [features]


# ### Provo 

# In[8]:


path_provo_data = '/mnt/ds3lab-scratch/noraho/cognitive-data/provo_gaze/Provo_Corpus-Eyetracking_Data.csv'


# In[9]:


with open(path_provo_data) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    header = next(csv_reader, None)
    
    for row in csv_reader:
        word = str(row[8]).lower()
        
        #total reading time = dwell_time
        trt = 0 if row[48] == 'NA' else row[48]
        
        #go past time = regression path duration
        gpt = 0 if row[58] == 'NA' else row[58]
        
        #first fixation duration
        ffd = 0 if row[35] == 'NA' else row[35]
        
        #gaze duration = first run dwell time
        gd = 0 if row[43] == 'NA' else row[43]
        
        #numFix = fixation count
        numFix = 0 if row[49] == 'NA' else row[49]
        
        #creating new feature: single fixation duration
        sfd = 0 if numFix != 1 else ffd
    
        features = np.array([float(trt), float(gpt), float(sfd), float(ffd), float(gd), float(numFix)])
        
        #including cog data of word in hashmap
        if word in word_dict.keys():
            word_dict[word].append(features)
                    
        else:
            word_dict[word] = [features]


# ### ZuCo 1.0 ET

# In[10]:


path_zuco1_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco1_preprocessed_sep2020/'


# In[11]:


for file_name in sorted(os.listdir(path_zuco1_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco1_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):
            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])

            for word_idx in range(numWords):

                #word string
                word_obj = file[sent['content'][word_idx][0]]
                word = u''.join(chr(c[0]) for c in word_obj).lower()
                #print(word)
                word = word.replace(",","")
                word = word.replace(".","")
                word = word.replace("(","")
                word = word.replace(")","")
                word = word.replace("\"","")
                #print(word)


                #ET features
                #initializing ET data variables
                ffd = gd = gpt = sfd = trt = numFix = None
                #ET measurements (FFD, GD, GPT, SFD, TRT, fixPositions)
                ref_ffd = sent['FFD'] #first fixation duration
                ffd = file[ref_ffd[word_idx][0]][()][0,0] if file[ref_ffd[word_idx][0]][()].shape == (1, 1) else None

                ref_gd = sent['GD'] #gaze duration
                gd = file[ref_gd[word_idx][0]][()][0,0] if file[ref_gd[word_idx][0]][()].shape == (1, 1) else None

                ref_gpt = sent['GPT'] #go-past time
                gpt = file[ref_gpt[word_idx][0]][()][0,0] if file[ref_gpt[word_idx][0]][()].shape == (1, 1) else None

                if('SFD' in sent.keys()): #there are words in ZuCo without SFD feature
                    ref_sfd = sent['SFD'] #single fixation duration
                    sfd = file[ref_sfd[word_idx][0]][()][0,0] if file[ref_sfd[word_idx][0]][()].shape == (1, 1) else None

                ref_trt = sent['TRT'] #total reading time
                trt = file[ref_trt[word_idx][0]][()][0,0] if file[ref_trt[word_idx][0]][()].shape == (1, 1) else None

                ref_numFix = sent['nFixations'] #number of word fixations
                numFix = file[ref_numFix[word_idx][0]][()][0,0] if file[ref_numFix[word_idx][0]][()].shape == (1, 1) else None

                feature_arr = [ffd, gd, gpt, sfd, trt, numFix]

                #skip/flag word if not all measurements are available
                flag = False
                for feature in feature_arr:
                    if feature == None:
                        flag = True

                if not flag:

                    features = np.array([float(trt), float(gpt), float(sfd), float(ffd), float(gd), float(numFix)])

                    #including cog data of word in hashmap
                    if word in word_dict.keys():
                        word_dict[word].append(features)

                    else:
                        word_dict[word] = [features]


# ### ZuCo 2.0 ET

# In[12]:


path_zuco2_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco2_preprocessed_sep2020/'


# In[13]:


for file_name in sorted(os.listdir(path_zuco2_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco2_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):
            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])

            for word_idx in range(numWords):

                #word string
                word_obj = file[sent['content'][word_idx][0]]
                word = u''.join(chr(c[0]) for c in word_obj).lower()
                #print(word)
                word = word.replace(",","")
                word = word.replace(".","")
                word = word.replace("(","")
                word = word.replace(")","")
                word = word.replace("\"","")
                #print(word)


                #ET features
                #initializing ET data variables
                ffd = gd = gpt = sfd = trt = numFix = None
                #ET measurements (FFD, GD, GPT, SFD, TRT, fixPositions)
                ref_ffd = sent['FFD'] #first fixation duration
                ffd = file[ref_ffd[word_idx][0]][()][0,0] if file[ref_ffd[word_idx][0]][()].shape == (1, 1) else None

                ref_gd = sent['GD'] #gaze duration
                gd = file[ref_gd[word_idx][0]][()][0,0] if file[ref_gd[word_idx][0]][()].shape == (1, 1) else None

                ref_gpt = sent['GPT'] #go-past time
                gpt = file[ref_gpt[word_idx][0]][()][0,0] if file[ref_gpt[word_idx][0]][()].shape == (1, 1) else None

                if('SFD' in sent.keys()): #there are words in ZuCo without SFD feature
                    ref_sfd = sent['SFD'] #single fixation duration
                    sfd = file[ref_sfd[word_idx][0]][()][0,0] if file[ref_sfd[word_idx][0]][()].shape == (1, 1) else None

                ref_trt = sent['TRT'] #total reading time
                trt = file[ref_trt[word_idx][0]][()][0,0] if file[ref_trt[word_idx][0]][()].shape == (1, 1) else None

                ref_numFix = sent['nFixations'] #number of word fixations
                numFix = file[ref_numFix[word_idx][0]][()][0,0] if file[ref_numFix[word_idx][0]][()].shape == (1, 1) else None

                feature_arr = [ffd, gd, gpt, sfd, trt, numFix]

                #skip/flag word if not all measurements are available
                flag = False
                for feature in feature_arr:
                    if feature == None:
                        flag = True

                if not flag:

                    features = np.array([float(trt), float(gpt), float(sfd), float(ffd), float(gd), float(numFix)])

                    #including cog data of word in hashmap
                    if word in word_dict.keys():
                        word_dict[word].append(features)

                    else:
                        word_dict[word] = [features]


# ### Min Max Scaler

# In[14]:


from sklearn import preprocessing


# In[15]:


final_word_dict = {}
for word in word_dict.keys():
    final_word_dict[word] = np.mean(preprocessing.minmax_scale(word_dict[word]), axis=0)


# ### Store Mapping

filename_store = "hashmap_ET_all.pickle"
file_store = open(filename_store, 'w')
file_store.close()
with open(filename_store, 'wb') as f:
    #dumping hashmap with mapping (word -> cognitive data features)
    pk.dump(final_word_dict, f)
    print(f"hashmap dumped in {filename_store}")




# In[21]:


cogId2vecMap = dict()
word2cogidMap = dict()
i = 0

for word in final_word_dict.keys():
    word2cogidMap[word] = i
    cogId2vecMap[i] = list(final_word_dict[word])
    i += 1


# In[24]:


#storing hashmap
dataset_name_store = 'cogid2vecMap_et_all.pickle'
with open(dataset_name_store, 'wb') as file:
    pk.dump(cogId2vecMap, file)
    
dataset_name_store = 'word2cogidMap_et_all.pickle'
with open(dataset_name_store, 'wb') as file:
    pk.dump(word2cogidMap, file)

