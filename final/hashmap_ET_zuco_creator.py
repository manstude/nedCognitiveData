#!/usr/bin/env python
# coding: utf-8

# In[21]:


import os
import numpy as np
import h5py
import pickle as pk


# In[6]:


word_dict = {}


# ### ZuCo 1.0 ET

# In[7]:


path_zuco1_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco1_preprocessed_sep2020/'


# In[10]:


for file_name in sorted(os.listdir(path_zuco1_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco1_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):
            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])

            for word_idx in range(numWords):

                #word string
                word_obj = file[sent['content'][word_idx][0]]
                word = u''.join(chr(c[0]) for c in word_obj).lower()
                #print(word)
                word = word.replace(",","")
                word = word.replace(".","")
                word = word.replace("(","")
                word = word.replace(")","")
                word = word.replace("\"","")
                #print(word)


                #ET features
                #initializing ET data variables
                ffd = gd = gpt = sfd = trt = numFix = None
                #ET measurements (FFD, GD, GPT, SFD, TRT, fixPositions)
                ref_ffd = sent['FFD'] #first fixation duration
                ffd = file[ref_ffd[word_idx][0]][()][0,0] if file[ref_ffd[word_idx][0]][()].shape == (1, 1) else None

                ref_gd = sent['GD'] #gaze duration
                gd = file[ref_gd[word_idx][0]][()][0,0] if file[ref_gd[word_idx][0]][()].shape == (1, 1) else None

                ref_gpt = sent['GPT'] #go-past time
                gpt = file[ref_gpt[word_idx][0]][()][0,0] if file[ref_gpt[word_idx][0]][()].shape == (1, 1) else None

                if('SFD' in sent.keys()): #there are words in ZuCo without SFD feature
                    ref_sfd = sent['SFD'] #single fixation duration
                    sfd = file[ref_sfd[word_idx][0]][()][0,0] if file[ref_sfd[word_idx][0]][()].shape == (1, 1) else None

                ref_trt = sent['TRT'] #total reading time
                trt = file[ref_trt[word_idx][0]][()][0,0] if file[ref_trt[word_idx][0]][()].shape == (1, 1) else None

                ref_numFix = sent['nFixations'] #number of word fixations
                numFix = file[ref_numFix[word_idx][0]][()][0,0] if file[ref_numFix[word_idx][0]][()].shape == (1, 1) else None

                feature_arr = [ffd, gd, gpt, sfd, trt, numFix]

                #skip/flag word if not all measurements are available
                flag = False
                for feature in feature_arr:
                    if feature == None:
                        flag = True

                if not flag:

                    features = np.array([float(trt), float(gpt), float(sfd), float(ffd), float(gd), float(numFix)])

                    #including cog data of word in hashmap
                    if word in word_dict.keys():
                        word_dict[word].append(features)

                    else:
                        word_dict[word] = [features]


# ### ZuCo 2.0 ET

# In[11]:


path_zuco2_data = '/mnt/ds3lab-scratch/noraho/datasets/zuco/zuco2_preprocessed_sep2020/'


# In[12]:


for file_name in sorted(os.listdir(path_zuco2_data)):
    if file_name.endswith('NR.mat'):
        print("reading from: " + file_name)
        file = f = h5py.File(path_zuco2_data + file_name, 'r')
        sentenceData = file['sentenceData']
        sentences = sentenceData['word']

        for sent_idx in range(sentences.shape[0]):
            sent = file[sentences[sent_idx][0]] #105 cog measurements

            #check if measurements were taken for this sentence
            if(np.array(sent).shape == (1, 1)):
                #skip this sentence for this subject, if cog data is not available
                continue

            numWords = len(sent['content'])

            for word_idx in range(numWords):

                #word string
                word_obj = file[sent['content'][word_idx][0]]
                word = u''.join(chr(c[0]) for c in word_obj).lower()
                #print(word)
                word = word.replace(",","")
                word = word.replace(".","")
                word = word.replace("(","")
                word = word.replace(")","")
                word = word.replace("\"","")
                #print(word)


                #ET features
                #initializing ET data variables
                ffd = gd = gpt = sfd = trt = numFix = None
                #ET measurements (FFD, GD, GPT, SFD, TRT, fixPositions)
                ref_ffd = sent['FFD'] #first fixation duration
                ffd = file[ref_ffd[word_idx][0]][()][0,0] if file[ref_ffd[word_idx][0]][()].shape == (1, 1) else None

                ref_gd = sent['GD'] #gaze duration
                gd = file[ref_gd[word_idx][0]][()][0,0] if file[ref_gd[word_idx][0]][()].shape == (1, 1) else None

                ref_gpt = sent['GPT'] #go-past time
                gpt = file[ref_gpt[word_idx][0]][()][0,0] if file[ref_gpt[word_idx][0]][()].shape == (1, 1) else None

                if('SFD' in sent.keys()): #there are words in ZuCo without SFD feature
                    ref_sfd = sent['SFD'] #single fixation duration
                    sfd = file[ref_sfd[word_idx][0]][()][0,0] if file[ref_sfd[word_idx][0]][()].shape == (1, 1) else None

                ref_trt = sent['TRT'] #total reading time
                trt = file[ref_trt[word_idx][0]][()][0,0] if file[ref_trt[word_idx][0]][()].shape == (1, 1) else None

                ref_numFix = sent['nFixations'] #number of word fixations
                numFix = file[ref_numFix[word_idx][0]][()][0,0] if file[ref_numFix[word_idx][0]][()].shape == (1, 1) else None

                feature_arr = [ffd, gd, gpt, sfd, trt, numFix]

                #skip/flag word if not all measurements are available
                flag = False
                for feature in feature_arr:
                    if feature == None:
                        flag = True

                if not flag:

                    features = np.array([float(trt), float(gpt), float(sfd), float(ffd), float(gd), float(numFix)])

                    #including cog data of word in hashmap
                    if word in word_dict.keys():
                        word_dict[word].append(features)

                    else:
                        word_dict[word] = [features]


# ### Min Max Scaler

# In[13]:


from sklearn import preprocessing


# In[14]:


final_word_dict = {}
for word in word_dict.keys():
    final_word_dict[word] = np.mean(preprocessing.minmax_scale(word_dict[word]), axis=0)


# ### Store Mapping

# In[16]:


cogId2vecMap = dict()
word2cogidMap = dict()
i = 0

for word in final_word_dict.keys():
    word2cogidMap[word] = i
    cogId2vecMap[i] = list(final_word_dict[word])
    i += 1


# In[22]:


#storing hashmap
dataset_name_store = 'cogid2vecMap_et_zuco.pickle'
with open(dataset_name_store, 'wb') as file:
    pk.dump(cogId2vecMap, file)
    
dataset_name_store = 'word2cogidMap_et_zuco.pickle'
with open(dataset_name_store, 'wb') as file:
    pk.dump(word2cogidMap, file)

