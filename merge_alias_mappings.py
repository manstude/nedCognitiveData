import numpy as np
import os
from tqdm import tqdm
from collections import defaultdict
import jsonlines
import random
import ujson
from bootleg.symbols.entity_symbols import EntitySymbols

#larger map
load_dir = '/mnt/ds3lab-scratch/manstude/data2train/eyetracking/entity_db/entity_mappings'
entity_symbols = EntitySymbols(load_dir=load_dir)

all_qids_with_aliases = set()
for al in tqdm(entity_symbols.get_all_aliases(), desc="Get all qids"):
    for qid in entity_symbols.get_qid_cands(al):
        all_qids_with_aliases.add(qid)

#alias output file from gen_alias_cand_map.py
train_alias_map = '/mnt/ds3lab-scratch/manstude/data2train/eyetracking/alias2qids_eye.json'
with open(train_alias_map) as f:
    train_aliases = ujson.load(f)

all_qids = set()
bad_qids = set()
for al in tqdm(train_aliases):
    for pair in train_aliases[al]:
        if not pair[0] in all_qids_with_aliases:
            bad_qids.add(pair[0])
        else:
            all_qids.add(pair[0])
print(f"Found a total of {len(all_qids)} good QIDs with {len(bad_qids)} bad ones out of {len(entity_symbols.get_all_qids())}")


train_data_f = '/mnt/ds3lab-scratch/manstude/data2train/eyetracking/train_wikiDataset.jsonl'
train_data = []
with jsonlines.open(train_data_f) as in_f:
    for line in in_f:
        train_data.append(line)
print(f"Found {len(train_data)} lines")


train_data_out_f = '/mnt/ds3lab-scratch/manstude/data2train/eyetracking/train.jsonl'
new_map = '/mnt/ds3lab-scratch/manstude/data2train/eyetracking/entity_db/entity_mappings'

# Compute original average candidate list size
total_cand_list_size = 0
for al in tqdm(entity_symbols.get_all_aliases(), desc="Getting cand sizes orig"):
    total_cand_list_size += len(entity_symbols.get_qid_count_cands(al))
print(f"Average candidate list size before filtering {total_cand_list_size/len(entity_symbols.get_all_aliases())}")


# Filter old aliases to only have qids in the data
alias2qids = defaultdict(list)
for al in tqdm(entity_symbols.get_all_aliases(), desc="Iterating aliases"):
    for pair in entity_symbols.get_qid_count_cands(al):
        if pair[0] in all_qids:
            alias2qids[al] = entity_symbols.get_qid_count_cands(al)
            break
print(f"Have {len(alias2qids)} new aliases")


all_qids_post_filter = set()
bad_qids_post_filter = set()
for al in tqdm(alias2qids):
    for pair in alias2qids[al]:
        if not pair[0] in all_qids_with_aliases:
            bad_qids_post_filter.add(pair[0])
        else:
            all_qids_post_filter.add(pair[0])
print(f"Found a total of {len(all_qids_post_filter)} good QIDs with {len(bad_qids_post_filter)} bad ones")


total_cand_list_size = 0
max_candidate_len = 0
for al in tqdm(alias2qids, desc="Getting cand sizes filtered"):
    total_cand_list_size += len(alias2qids[al])
    max_candidate_len = max(max_candidate_len, len(alias2qids[al]))
print(f"Average candidate list size after filtering {total_cand_list_size/len(alias2qids)} with max {max_candidate_len}")





# Get reverse mapping so we can find good aliases to use for QIDs where the alias isn't in the mapping or the conflict is too low
qid2aliases = defaultdict(set)
qid2aliaseswithlen = defaultdict(set)
for al in tqdm(alias2qids, desc="Building inv mapping"):
    for pair in alias2qids[al]:
        qid2aliases[pair[0]].add(al)
        qid2aliaseswithlen[pair[0]].add(tuple([al, len(alias2qids[al])]))


# Filter training data
skipped = 0
single_cand = 0
all_mens = 0
with jsonlines.open(train_data_out_f, "w") as out_f:
    for line in tqdm(train_data, desc="Modifying train data"):
        new_aliases = []
        new_qids = []
        new_spans = []
        new_gold = []
        for i, (al, qid, sp, g) in enumerate(zip(line["aliases"], line["qids"], line["spans"], line["gold"])):
            if qid in bad_qids or sp[1] > len(line["sentence"].split()):
                skipped += 1
                continue
            all_mens += 1
            new_qids.append(qid)
            new_spans.append(sp)
            new_gold.append(g)
            # If len candidate list is 1 or the alias isn't mapping to that qid, swap
            if len(alias2qids[al]) == 1 or al not in qid2aliases[qid]:
                possible_aliases = [pair for pair in qid2aliaseswithlen[qid] if pair[1] > 1]
                if len(possible_aliases) == 0:
                    single_cand += 1
                    # Take any aliases then
                    new_alias = list(qid2aliaseswithlen[qid])[0][0]
                    new_aliases.append(new_alias)
                else:
                    new_alias = random.choices(population=[p[0] for p in possible_aliases],
                                                      weights=[p[1] for p in possible_aliases],
                                                      k=1)[0]
                    new_aliases.append(new_alias)
            else:
                new_aliases.append(al)
        if len(new_aliases) <= 0:
            continue
        line["aliases"] = new_aliases
        line["qids"] = new_qids
        line["spans"] = new_spans
        line["gold"] = new_gold
        out_f.write(line)
print(f"Dropped {skipped} mentions with a total of {single_cand} qids single out of {all_mens}")




# Get new qid2title mapping
qid2title = {}
for qid in all_qids_post_filter:
    qid2title[qid] = entity_symbols.get_title(qid)
print(f"Final num qids {len(qid2title)}")



new_entity_symbols = EntitySymbols(max_candidates=max_candidate_len, max_alias_len=entity_symbols.max_alias_len,
        alias2qids=alias2qids, qid2title=qid2title, alias_cand_map_file="alias2qids.json")
new_entity_symbols.dump(save_dir=new_map)

